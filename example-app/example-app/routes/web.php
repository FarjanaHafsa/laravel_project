<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', function () {
    return view('admin.home');
})->name('home');
Route::get('/product-add', function () {
    return view('admin.product-add');
})->name('product-add');
Route::get('/product-list', function () {
    return view('admin.product-list');
})->name('product');
Route::get('/product-edit', function () {
    return view('admin.product-edit');
})->name('product-edit');
Route::get('/profile', function () {
    return view('admin.profile');
})->name('profile');
Route::get('/category-add', function () {
    return view('admin.category-add');
})->name('category-add');
Route::get('/category-edit', function () {
    return view('admin.category-edit');
})->name('category-edit');
Route::get('/category-list', function () {
    return view('admin.category-list');
})->name('category');
Route::get('/view-category', function () {
    return view('admin.view-category');
});
Route::get('/view-product', function () {
    return view('admin.view-product');
})->name('view-product');

//fontend

Route::get('/index', function () {
    return view('fontend.index');
})->name('index');
Route::get('/about', function () {
    return view('fontend.about');
})->name('about');
Route::get('/contact', function () {
    return view('fontend.contact');
})->name('contact');
Route::get('/checkout', function () {
    return view('fontend.checkout');
})->name('checkout');
Route::get('/cart', function () {
    return view('fontend.cart');
})->name('cart');

Route::get('/shop', function () {
    return view('fontend.shop');
})->name('shop');
Route::get('/registation', function () {
    return view('fontend.registation');
})->name('registation');

Route::get('/login', function () {
    return view('fontend.login');
})->name('login');
Route::get('/invoice', function () {
    return view('fontend.invoice');
})->name('invoice');
Route::get('/product-details', function () {
    return view('fontend.product-details');
})->name('product-details');

Route::get('/thank-you', function () {
    return view('fontend.thank-you');
})->name('thank-you');
