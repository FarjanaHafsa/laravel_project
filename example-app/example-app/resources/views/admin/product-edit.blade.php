<x-master>
      <!-- View Product Area Start  -->
      <div class="product-section section py-4">
        <div class="container-fluid pt-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="product-list.html">Product</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Product-Edit</li>
                </ol>
              </nav>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 mb-30">
                    <div class="product-table">
                        <form action="">
                            <table>
                                <thead>
                                    <tr>
                                        <th><label for="pname">Product Name</label></th>
                                        <th><label for="pcategory">Category Name</label></th>
                                        <th><label for="pprice">Price</label></th>
                                        <th><label for="pimages">Images</label></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="text" name="pname" id="pname"></td>
                                        <td><input type="text" name="pcategory" id="pcategory"></td>
                                        <td><input type="text" name="pprice" id="pprice"></td>
                                        <td><input type="file" name="pimages" id="pimages"></td>
                                    </tr>
                                    <tr>
                                        <td><input class="psubmit" type="submit" name="psubmit" id="psubmit" value="Save Product"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
      </div>
        <!-- View Product Area End  -->

</x-master>