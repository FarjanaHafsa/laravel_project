﻿<x-master>
  
 <!-- Dashbord Area Start -->
 <div class="items">
          <div class="container-fluid">
            <div class="row py-3">
              <div class="col-md-3">
                <div class="single-item">
                  <div class="content-item">
                    <h5>54</h5>
                    <p>Custom</p>
                  </div>
                  <i class="fa-solid fa-users"></i>
                </div>
              </div>
              <div class="col-md-3">
                <div class="single-item">
                  <div class="content-item">
                    <h5>104</h5>
                    <p>Products</p>
                  </div>
                  <i class="fa-solid fa-briefcase"></i>
                </div>
              </div>
              <div class="col-md-3">
                <div class="single-item">
                  <div class="content-item">
                    <h5>34</h5>
                    <p>Orders</p>
                  </div>
                  <i class="fa-solid fa-bus"></i>
                </div>
              </div>
              <div class="col-md-3">
                <div class="single-item">
                  <div class="content-item">
                    <h5>320</h5>
                    <p>Income</p>
                  </div>
                  <i class="fa-solid fa-money-check-dollar"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Dashbord Area End -->

        <!-- Table Start  -->
        <div class="table-area ">
          <div class="container-fluid pb-3 px-3">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">No</th>
                  <th scope="col">First</th>
                  <th scope="col">Last</th>
                  <th scope="col">User</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>@mdo</td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Jacob</td>
                  <td>Thornton</td>
                  <td>@fat</td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td colspan="2">Larry the Bird</td>
                  <td>@twitter</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div-table-area>
        <!-- Table End  -->


</x-master>