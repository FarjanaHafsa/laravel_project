<x-master>
        <!-- Header Area End -->

        <!-- View Product Area Start  -->
    <div class="product-section section py-2">
        <div class="container-fluid pt-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="product-list.html">Product</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Product-View</li>
                </ol>
              </nav>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 col-12 mb-30">
                    <div class="single-product-image product-image-slider fix">
                        <div class="single-image"><img src="admin/images/fish-cat.jpg" alt="fish-cat"></div>
                    </div>
                </div>
                <div class="single-product-content col-lg-7 col-12 mb-30">
                    <h1 class="title">Organic Surma Dry Fish</h1>
                    <div class="description">
                        <p>There are many variations of passages of Lorem Ipsum avaable,majority have suffered alteration in some form, by injected humour, or rdomised words which don't look even slightly believable.</p>
                    </div>
                    <span class="product-price">1295 Tk</span>
                    <!-- Quantity & Cart Button -->
                    <div class="product-quantity-cart fix">
                        <div class="product-quantity">
                            <span class="dec qtybtn"><i class="fa-solid fa-angle-left"></i></span>
                            <input type="number" value="0" name="qtybox">
                            <span class="inc qtybtn"><i class="fa-solid fa-angle-right"></i></span>
                        </div>
                        <button class="add-to-cart">add to cart</button>
                    </div>
                    <!-- Action Button -->
                    <div class="product-action-button fix">
                        <ul>
                            <li><span>Categories:</span><a href="#"> Fish</a></li>
                        </ul>
                    </div>
                </div>
                
            </div>
            <!-- Product Additional Info Start-->
            <div class="row py-5">
                <div class="col-md-12 ">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                          <h2 class="accordion-header" id="headingOne">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                DESCRIPTION
                            </button>
                          </h2>
                          <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                              <strong>This is the first item's accordion body.</strong> It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                            </div>
                          </div>
                        </div>
                        <div class="accordion-item">
                          <h2 class="accordion-header" id="headingTwo">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                QUESTIONS ABOUT THIS PRODUCT
                            </button>
                          </h2>
                          <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                              <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
                            </div>
                          </div>
                        </div>
                      </div>
                </div>
            </div>
            
        </div>
    </div>
        <!-- View Product Area End  -->

</x-master>