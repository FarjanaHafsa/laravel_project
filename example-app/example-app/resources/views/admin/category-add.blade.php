<x-master>
        <!-- Header Area End -->

        <!-- View Product Area Start  -->
    <div class="product-section section py-4">
        <div class="container-fluid pt-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="category-list.html">Category</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Category-Add</li>
                </ol>
              </nav>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-12 mb-30">
                    <div class="categori-table">
                        <form action="">
                            <table>
                                <thead>
                                    <tr>
                                        <th><label for="ccategory">Category Name</label></th>
                                        <th><label for="cimages">Images</label></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="text" name="ccategory" id="ccategory"></td>
                                        <td><input type="file" name="cimages" id="cimages"></td>
                                    </tr>
                                    <tr>
                                        <td><input class="csubmit" type="submit" name="csubmit" id="csubmit" value="Save Category"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
      </div>
        <!-- View Product Area End  -->

</x-master>