<x-master>
        <!-- Header Area End -->

        <!-- View Product Area Start  -->
    <div class="product-section section py-4">
        <div class="container-fluid pt-5">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html">Dashbord</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Profile</li>
                </ol>
              </nav>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-12">
                    <div class="profile-left">
                        <img src="admin/images/azaz-ahmed.JPG" width="250px" height="300px" alt="azaz-ahmed">
                        <div class="profile-content">
                            <h2>Azaz Ahmed</h2>
                            <h3>Admin</h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-12">
                    <div class="profile-right">
                        <h3>Edit Profile</h3>
                        <form action="">
                            <table>
                                <thead>
                                    <tr>
                                        <th><label for="profilename">Profile Name</label></th>
                                        <th><label for="designation">Designation</label></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="text" name="profilename" id="profilename"></td>
                                        <td><input type="text" name="designation" id="designation"></td>
                                    </tr>
                                    <tr>
                                        <th><label for="oldpass">Old Password</label></th>
                                        <th><label for="newpass">New Password</label></th>
                                    </tr>
                                    <tr>
                                        <td><input type="text" name="oldpass" id="oldpass"></td>
                                        <td><input type="text" name="newpass" id="newpass"></td>
                                    </tr>
                                    <tr>
                                        <th colspan="2"><label for="profileimages">Profile Images</label></th>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input type="file" name="profileimages" id="profileimages"></td>
                                        <td><input class="passsubmit" type="submit" name="passsubmit" id="passsubmit" value="Save Password"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
      </div>
        <!-- View Product Area End  -->

</x-master>