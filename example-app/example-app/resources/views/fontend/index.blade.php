<x-layouts>
    <!-- Header Area End -->

    <!-- Bnner Area Start -->
    <section class="banner-area">
        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="{{ asset('fontend/images/banner-1.jpg')}}" class="d-block w-100 banner-hight" alt="banner-1">
              </div>
              <div class="carousel-item">
                <img src="{{ asset('fontend/images/banner-2.jpg')}}" class="d-block w-100 banner-hight" alt="banner-2">
              </div>
              <div class="carousel-item">
                <img src="{{ asset('fontend/images/banner-3.jpg')}}" class="d-block w-100 banner-hight" alt="banner-3">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
    </section>
    <!-- Bnner Area End -->

    <!-- Products Area Start  -->
    <section class="products-area py-4">
      <div class="container">
        <div class="main-title">
          <h2>PRODUCT CATEGORIES</h2>
        </div>
      </div>
      <div class="container">
        <div class="products-catagories">
          <div class="row">
            <div class="col-md-2">
              <div class="single-pro-cat">
                <img src="{{ asset('fontend/images/product-catagories/Fish.png')}}" alt="FISH">
                <div class="single-pro-cat-content">
                  <h3>FISH</h3>
                  <p>10 products</p>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="single-pro-cat">
                <img src="{{ asset('fontend/images/product-catagories/Fruits.png')}}" alt="Fruits">
                <div class="single-pro-cat-content">
                  <h3>Fruits</h3>
                  <p>15 products</p>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="single-pro-cat">
                <img src="{{ asset('fontend/images/product-catagories/Grocery.png')}}" alt="Grocery">
                <div class="single-pro-cat-content">
                  <h3>Grocery</h3>
                  <p>5 products</p>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="single-pro-cat">
                <img src="{{ asset('fontend/images/product-catagories/Honey.png')}}" alt="Honey">
                <div class="single-pro-cat-content">
                  <h3>Honey</h3>
                  <p>20 products</p>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="single-pro-cat">
                <img src="{{ asset('fontend/images/product-catagories/Meat-2.png')}}" alt="Meat">
                <div class="single-pro-cat-content">
                  <h3>Meat</h3>
                  <p>5 products</p>
                </div>
              </div>
            </div>
            <div class="col-md-2">
              <div class="single-pro-cat">
                <img src="{{ asset('fontend/images/product-catagories/Dairy-01a.png')}}" alt="Dairy">
                <div class="single-pro-cat-content">
                  <h3>Dairy</h3>
                  <p>5 products</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Products Area End  -->

  <!-- Our Products Area Start  -->
  <section class="products-area py-4">
    <div class="container">
      <div class="main-title">
        <h2>OUR PRODUCT</h2>
      </div>
    </div>
    <div class="container">
      <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="products-items">
              <div class="row">
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{ asset('fontend/images/product/Beef-Kalabhuna-Masala-100gm-300x300.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Beef Kalabhuna</h5>
                      <p class="card-text">500 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{asset('fontend/images/product/dim-300x300.png')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Dim</h5>
                      <p class="card-text">120 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{ asset('fontend/images/product/Milk-E-300x300.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Milk</h5>
                      <p class="card-text">500 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{ asset('fontend/images/product/New-Project-3-300x300.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">New</h5>
                      <p class="card-text">500 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="products-items">
              <div class="row">
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{ asset('fontend/images/product/Beef-Kalabhuna-Masala-100gm-300x300.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Beef Kalabhuna</h5>
                      <p class="card-text">500 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{ asset('fontend/images/product/dim-300x300.png')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Dim</h5>
                      <p class="card-text">120 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{asset('fontend/images/product/Milk-E-300x300.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Milk</h5>
                      <p class="card-text">500 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{asset('fontend/images/product/New-Project-3-300x300.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">New</h5>
                      <p class="card-text">500 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="products-items">
              <div class="row">
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{ asset('fontend/images/product/Beef-Kalabhuna-Masala-100gm-300x300.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Beef Kalabhuna</h5>
                      <p class="card-text">500 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{ asset('fontend/images/product/dim-300x300.png')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Dim</h5>
                      <p class="card-text">120 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{ asset('fontend/images/product/Milk-E-300x300.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">Milk</h5>
                      <p class="card-text">500 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="card" style="width: 18rem;">
                    <img src="{{ asset('fontend/images/product/New-Project-3-300x300.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">New</h5>
                      <p class="card-text">500 Tk</p>
                      <a href="#" class="btn btn-primary">Add to Cart</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
    </div>
  </section>
  <!-- Our Products Area End  -->

  <!-- Testimonials Area Start -->
  <section class="testimonials-area py-4">
    <div class="container">
      <div class="main-title">
        <h2>Testimonials</h2>
      </div>
    </div>
    <div class="container-fluid pt-4">
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img class="tes-img" src="{{ asset('fontend/images/testimonials/tes-1.jpg')}}" alt="">
                        </div>
                        <div class="col-md-8">
                            <div class="test-text">
                                <h1>“</h1>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here,
                                 content here', making it look like readable English.</p>
                            <span>Abdus Salam</span>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img class="tes-img" src="{{ asset('fontend/images/testimonials/tes-2.jpg')}}" alt="">
                        </div>
                        <div class="col-md-8">
                            <div class="test-text">
                                <h1>“</h1>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, 
                                content here', making it look like readable English.</p>
                            <span>Eyamin Hossain</span>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <img class="tes-img" src="{{ asset('fontend/images/testimonials/test-3.jpg')}}" alt="">
                        </div>
                        <div class="col-md-8">
                            <div class="test-text">
                                <h1>“</h1>
                            <p>Hello, I don’t work at Intel. But I buy my glasses from Glassesbd.com</p>
                            <span>Raef Al Hasan Rafa </span>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
    </div>
</section>
<!-- Testimonials Area End -->

    <!-- Footer Area Start -->
</x-layouts>