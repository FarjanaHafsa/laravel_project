<x-layouts>
    <!-- Header Area End -->

    <!-- Page Banner Section Start-->
    <div class="page-banner-section section" style="background-image: url(fontend/images/page-banner.jpg)">
        <div class="container">
            <div class="row">
                
                <!-- Page Title Start -->
                <div class="page-title text-center col">
                    <h1>Cart</h1>
                </div><!-- Page Title End -->
                
            </div>
        </div>
    </div><!-- Page Banner Section End-->

    <!-- Cart Section Start-->
    <div class="cart-section section py-4 pb-90">
        <div class="container">
            <div class="row">
                <div class="col-12">
                   
                    <div class="table-responsive pb-4">
                        <table class="table cart-table text-center">
                            
                            <!-- Table Head -->
                            <thead>
                                <tr>
                                    <th class="number">No</th>
                                    <th class="image">image</th>
                                    <th class="name">product name</th>
                                    <th class="qty">quantity</th>
                                    <th class="price">price</th>
                                    <th class="total">totle</th>
                                    <th class="remove">remove</th>
                                </tr>
                            </thead>
                            
                            <!-- Table Body -->
                            <tbody>
                                <tr>
                                    <td><span class="cart-number">1</span></td>
                                    <td><a href="#" class="cart-pro-image"><img src="{{ asset('fontend/images/product/Beef-Kalabhuna-Masala-100gm-300x300.jpg')}}" alt="" /></a></td>
                                    <td><a href="#" class="cart-pro-title">Beef</a></td>
                                    <td><div class="product-quantity">
                                        <span class="dec qtybtn"><i class="fa-solid fa-angle-left"></i></span>
                                        <input type="number" value="0" name="qtybox">
                                        <span class="inc qtybtn"><i class="fa-solid fa-angle-right"></i></span>
                                    </div></td>
                                    <td><p class="cart-pro-price">700 Tk</p></td>
                                    <td><p class="cart-price-total">700 Tk</p></td>
                                    <td><button class="cart-pro-remove"><i class="fa-solid fa-trash-can"></i></button></td>
                                </tr>
                                <tr>
                                    <td><span class="cart-number">2</span></td>
                                    <td><a href="#" class="cart-pro-image"><img src="{{ asset('fontend/images/product/dim-300x300.png')}}" alt="" /></a></td>
                                    <td><a href="#" class="cart-pro-title">Dim</a></td>
                                    <td><div class="product-quantity">
                                        <span class="dec qtybtn"><i class="fa-solid fa-angle-left"></i></span>
                                        <input type="number" value="0" name="qtybox">
                                        <span class="inc qtybtn"><i class="fa-solid fa-angle-right"></i></span>
                                    </div></td>
                                    <td><p class="cart-pro-price">120 Tk</p></td>
                                    <td><p class="cart-price-total">120 Tk</p></td>
                                    <td><button class="cart-pro-remove"><i class="fa-solid fa-trash-can"></i></button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                    <div class="row">

                        <!-- Cart Cuppon -->
                        <div class="cart-cuppon col-lg-6 col-md-6 col-12 mb-30">
                            <h4 class="title">Discount Code</h4>
                            <p>Enter your coupon code if you have</p>
                            <form action="#" class="cuppon-form">
                                <input type="text" placeholder="Cuppon Code">
                                <button class="button">apply coupon</button>
                            </form>
                        </div>
                        
                        <!-- Cart Checkout Progress -->
                        <div class="cart-checkout-process col-lg-6 col-md-6 col-12 mb-30">
                            <h4 class="title">Process Checkout</h4>
                            <p><span>Subtotal</span><span>820 Tk</span></p>
                            <h5><span>Grand total</span><span>820 Tk</span></h5>
                            <button class="button"><a style="color: #fff;" href="{{ route('checkout')}}">process to checkout</a></button>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div><!-- Cart Section End-->

    <!-- Footer Area Start -->
</x-layouts>