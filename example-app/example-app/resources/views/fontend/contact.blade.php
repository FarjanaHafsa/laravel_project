<x-layouts>
    <!-- Header Area End -->

    <!-- Page Banner Section Start-->
    <div class="page-banner-section section" style="background-image: url(fontend/images/page-banner.jpg)">
        <div class="container">
            <div class="row">
                
                <!-- Page Title Start -->
                <div class="page-title text-center col">
                    <h1>Contact us</h1>
                </div><!-- Page Title End -->
                
            </div>
        </div>
    </div><!-- Page Banner Section End-->

    <!-- Contact Section Start-->
    <div class="contact-section py-4 section bg-white pt-120">
        <div class="container">
            <div class="row">
                
                <div class="col-xl-10 col-12 offset-xl-1">
                    
                    <div class="contact-wrapper">
                        <div class="row">

                            <div class="contact-info col-lg-5 col-12">
                                <h4 class="title">Contact Info</h4>
                                <p>It is a long established fact that readewill be distracted by the readable content of a page when looking at ilayout.</p>
                                <ul>
                                    <li><span>Address:</span>Mirpur 12</li>
                                    <li><span>Phone:</span>+8801756645566</li>
                                    <li><span>Email:</span>azazahmed623@email.com</li>
                                </ul>
                                <div class="contact-social">
                                    <li><a href="#"><i class="fa-brands fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa-brands fa-square-youtube"></i></a></li>
                                    <li><a href="#"><i class="fa-brands fa-square-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa-brands fa-square-twitter"></i></a></li>
                                </div>
                            </div>
                            <div class="contact-form col-lg-7 col-12">
                                <h4 class="title">Send Your Massage</h4>
                                <form id="contact-form" action="https://demo.hasthemes.com/christ-preview/christ/mail.php" method="post">
                                    <input type="text" name="name" placeholder="Your Name">
                                    <input type="email" name="email" placeholder="Your Email">
                                    <textarea name="message" placeholder="Your Message"></textarea>
                                    <input type="submit" value="Submit">
                                </form>
                                <p class="form-messege"></p>
                            </div>

                        </div>
                    </div>
                    
                </div>
            
            </div>
        </div>
    </div>
    <!-- Contact Section End-->

    <!-- Footer Area Start -->
 </x-layouts>