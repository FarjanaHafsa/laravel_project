<x-layouts>
    <!-- Header Area End -->

    <!-- Page Banner Section Start-->
    <div class="page-banner-section section" style="background-image: url(fontend/images/page-banner.jpg)">
        <div class="container">
            <div class="row">
                
                <!-- Page Title Start -->
                <div class="page-title text-center col">
                    <h1>CHECKOUT</h1>
                </div><!-- Page Title End -->
                
            </div>
        </div>
    </div><!-- Page Banner Section End-->

    <!-- Checkout Section Start-->
    <div class="cart-section section py-4 pb-90">
        <div class="container">
            <div class="row">
               
                <div class="col-lg-6 col-12 mb-30 order-details-wrapper">
                    <h2 class="checkout-title">Billing Details</h2><!-- End .checkout-title -->
                    <!-- Checkout Accordion Start -->
                    <form action="#">
                        <div class="row">
                            <div class="col-lm-9">
                                
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>First Name *</label>
                                            <input type="text" class="form-control" required>
                                        </div><!-- End .col-sm-6 -->

                                        <div class="col-sm-6">
                                            <label>Last Name *</label>
                                            <input type="text" class="form-control" required>
                                        </div><!-- End .col-sm-6 -->
                                    </div><!-- End .row -->

                                    <label>Company Name (Optional)</label>
                                    <input type="text" class="form-control">

                                    <label>Country *</label>
                                    <input type="text" class="form-control" required>

                                    <label>Street address *</label>
                                    <input type="text" class="form-control" placeholder="House number and Street name" required>
                                    <input type="text" class="form-control" placeholder="Appartments, suite, unit etc ..." required>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Town / City *</label>
                                            <input type="text" class="form-control" required>
                                        </div><!-- End .col-sm-6 -->

                                        <div class="col-sm-6">
                                            <label>State / County *</label>
                                            <input type="text" class="form-control" required>
                                        </div><!-- End .col-sm-6 -->
                                    </div><!-- End .row -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Postcode / ZIP *</label>
                                            <input type="text" class="form-control" required>
                                        </div><!-- End .col-sm-6 -->

                                        <div class="col-sm-6">
                                            <label>Phone *</label>
                                            <input type="tel" class="form-control" required>
                                        </div><!-- End .col-sm-6 -->
                                    </div><!-- End .row -->

                                    <label>Email address *</label>
                                    <input type="email" class="form-control" required>

                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="checkout-create-acc">
                                        <label class="custom-control-label" for="checkout-create-acc">Create an account?</label>
                                    </div><!-- End .custom-checkbox -->

                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="checkout-diff-address">
                                        <label class="custom-control-label" for="checkout-diff-address">Ship to a different address?</label>
                                    </div><!-- End .custom-checkbox -->

                                    <label>Order notes (optional)</label>
                                    <textarea class="form-control" cols="30" rows="4" placeholder="Notes about your order, e.g. special notes for delivery"></textarea>
                            </div><!-- End .col-lg-9 -->

                        </div><!-- End .row -->
                    </form>
                    
                </div>
                
                <!-- Order Details -->
                <div class="col-lg-6 col-12 mb-30">
                   
                    <div class="order-details-wrapper">
                        <h2>your order</h2>
                        <div class="order-details">
                            <form action="#">
                                <ul>
                                    <li><p class="strong">product</p><p class="strong">total</p></li>
                                    <li><p>Holiday Candle x1</p><p>$104.99</p></li>
                                    <li><p>Christmas Tree x1 </p><p>$85.99</p></li>
                                    <li><p class="strong">cart subtotal</p><p class="strong">$190.98</p></li>
                                    <li><p class="strong">shipping</p><p>
                                        <input type="radio" name="order-shipping" id="flat" /><label for="flat">Flat Rate $ 7.00</label><br />
                                        <input type="radio" name="order-shipping" id="free" /><label for="free">Free Shipping</label>
                                    </p></li>
                                    <li><p class="strong">order total</p><p class="strong">$190.98</p></li>
                                    <li><button class="button">place order</button></li>
                                </ul>
                            </form>
                        </div>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div><!-- Checkout Section End-->

    <!-- Footer Area Start -->
</x-layouts>