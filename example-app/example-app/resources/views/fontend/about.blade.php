<x-layouts>
    <!-- Header Area End -->

    <!-- Page Banner Section Start-->
    <div class="page-banner-section section" style="background-image: url(fontend/images/page-banner.jpg)">
        <div class="container">
            <div class="row">
                
                <!-- Page Title Start -->
                <div class="page-title text-center col">
                    <h1>About us</h1>
                </div><!-- Page Title End -->
                
            </div>
        </div>
    </div><!-- Page Banner Section End-->

    <!-- About Section Start-->
    <div class="about-section section pt-4 pb-90">
        <div class="container">
            <div class="row flex-row-reverse">
                
                <!-- About Image -->
                <div class="about-image col-lg-6 col-12 mb-30">
                    <a class="video-popup" href="https://www.youtube.com/watch?v=7e90gBu4pas"><img src="images/about.jpg" alt=""></a>
                </div>
                
                <!-- Mission Content -->
                <div class="about-content col-lg-6 col-12 mb-30">
                    <h2>About</h2>
                    <p>There are many variations of passages of Lorem Ipsum available, majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly kn je believable There are manations of passages of Lorem Ipsum available, but the majority ahave suffered ami tar cholnay vulbo na alte ration. majority have suffered alteration in</p>
                    <a href="#" class="button">Shop Now</a>
                </div>
                
            </div>
        </div>
    </div><!-- About Section End-->

    <!-- Misson Vison Section Start-->
    <div class="about-section pb-4 section pt-120 pb-90">
        <div class="container">
            <div class="row flex-row-reverse">
                
                <!-- Mission Content -->
                <div class="about-content col-lg-6 col-12 mb-30">
                    <h2>Mission</h2>
                    <p>There are many variations of passages of Lorem Ipsum available, majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly kn je believable There are manations of passages of Lorem Ipsum available, but the majority ahave suffered ami tar cholnay vulbo na alte ration. majority have suffered alteration in</p>
                    
                </div>
                
                <!-- Vison Content -->
                <div class="about-content col-lg-6 col-12 mb-30">
                    <h2>Vision</h2>
                    <p>There are many variations of passages of Lorem Ipsum available, majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly kn je believable There are manations of passages of Lorem Ipsum available, but the majority ahave suffered ami tar cholnay vulbo na alte ration. majority have suffered alteration in</p>
                    
                </div>
                
            </div>
        </div>
    </div><!-- Misson Vison Section End-->

    <!-- Footer Area Start -->
</x-layouts>