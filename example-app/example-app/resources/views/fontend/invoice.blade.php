<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>DotNetTec - Invoice html template bootstrap</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/css/bootstrap.css'>
</head>
<body>
    <div class="container">
        <div class="card">
            <div class="card-header" style="background-color: green; color:#fff;">
                Invoice
                <strong>01/01/2020</strong>
                <span class="float-right"> <strong>Status:</strong> Pending</span>

            </div>
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col-md-4">
                        <h2>WARRIORS</h2>
                    </div>
                    <div class="col-md-4">
                        <h6 class="mb-3">From:</h6>
                        <div>
                            <strong>DotNetTec</strong>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <h6 class="mb-3">To:</h6>
                        <div>
                            <strong>Robert Maxwel</strong>
                        </div>
                    </div>
                </div>

                <div class="table-responsive-sm">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="center">No</th>
                                <th>Item</th>
                                <th>Description</th>

                                <th class="right">Unit Cost</th>
                                <th class="center">Qty</th>
                                <th class="right">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="center">1</td>
                                <td class="left strong">Beef</td>
                                <td class="left">Extended License</td>

                                <td class="right">700 Tk</td>
                                <td class="center">1</td>
                                <td class="right">700 Tk</td>
                            </tr>
                            <tr>
                                <td class="center">2</td>
                                <td class="left">Dim</td>
                                <td class="left">Instalation and Customization)</td>

                                <td class="right">120 Tk</td>
                                <td class="center">12</td>
                                <td class="right">120 Tk</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-sm-5">

                    </div>

                    <div class="col-lg-4 col-sm-5 ml-auto">
                        <table class="table table-clear">
                            <tbody>
                                <tr>
                                    <td class="left">
                                        <strong>Subtotal</strong>
                                    </td>
                                    <td class="right">820 Tk</td>
                                </tr>
                                <tr>
                                    <td class="left">
                                        <strong>Discount (20%)</strong>
                                    </td>
                                    <td class="right">820 Tk</td>
                                </tr>
                                <tr>
                                    <td class="left">
                                        <strong>VAT (10%)</strong>
                                    </td>
                                    <td class="right">820 Tk</td>
                                </tr>
                                <tr>
                                    <td class="left">
                                        <strong>Total</strong>
                                    </td>
                                    <td class="right">
                                        <strong>820 Tk</strong>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>