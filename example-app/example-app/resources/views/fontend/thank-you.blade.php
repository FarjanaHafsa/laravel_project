<x-layouts>
    <!-- Header Area End -->

    <!-- Page Banner Section Start-->
    <div class="page-banner-section section" style="background-image: url(fontend/images/page-banner.jpg)">
        <div class="container">
            <div class="row">
                
                <!-- Page Title Start -->
                <div class="page-title text-center col">
                    <h1>Thank You</h1>
                </div><!-- Page Title End -->
                
            </div>
        </div>
    </div><!-- Page Banner Section End-->

    <!-- About Section Start-->
    <div class="about-section section pt-4 pb-90">
        <div class="container">
            <div class="row flex-row-reverse">
                
                <!-- About Image -->
                <div class="about-content col-lg-12 ">
                    <h2>Thank You</h2>
                </div>
                
            </div>
        </div>
    </div><!-- About Section End-->

    

    <!-- Footer Area Start -->
</x-layouts>