<x-layouts>
    <!-- Header Area End -->

    <!-- Page Banner Section Start-->
    <div class="page-banner-section section" style="background-image: url(fontend/images/page-banner.jpg)">
        <div class="container">
            <div class="row">
                
                <!-- Page Title Start -->
                <div class="page-title text-center col">
                    <h1>SHOP PAGE</h1>
                </div><!-- Page Title End -->
                
            </div>
        </div>
    </div><!-- Page Banner Section End-->

    <!-- Shope Porducts Start  -->
    <div class="container pb-5">
    <div class="shope-row-1">
        <div class="products-items">
            <div class="row">
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{ asset('fontend/images/product/Beef-Kalabhuna-Masala-100gm-300x300.jpg')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Beef Kalabhuna</h5>
                    <p class="card-text">500 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{ asset('fontend/images/product/dim-300x300.png')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Dim</h5>
                    <p class="card-text">120 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{ asset('fontend/images/product/Milk-E-300x300.jpg')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Milk</h5>
                    <p class="card-text">500 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{ asset('fontend/images/product/New-Project-3-300x300.jpg')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">New</h5>
                    <p class="card-text">500 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
    <div class="shope-row-1">
        <div class="products-items">
            <div class="row">
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{ asset('fontend/images/product/Beef-Kalabhuna-Masala-100gm-300x300.jpg')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Beef Kalabhuna</h5>
                    <p class="card-text">500 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{ asset('fontend/images/product/dim-300x300.png')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Dim</h5>
                    <p class="card-text">120 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{ asset('fontend/images/product/Milk-E-300x300.jpg')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Milk</h5>
                    <p class="card-text">500 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{ asset('fontend/images/product/New-Project-3-300x300.jpg')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">New</h5>
                    <p class="card-text">500 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
    <div class="shope-row-1">
        <div class="products-items">
            <div class="row">
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{ asset('fontend/images/product/Beef-Kalabhuna-Masala-100gm-300x300.jpg')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Beef Kalabhuna</h5>
                    <p class="card-text">500 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{ asset('fontend/images/product/dim-300x300.png')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Dim</h5>
                    <p class="card-text">120 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{asset('fontend/images/product/Milk-E-300x300.jpg')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">Milk</h5>
                    <p class="card-text">500 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="card" style="width: 18rem;">
                  <img src="{{asset('fontend/images/product/New-Project-3-300x300.jpg')}}" class="card-img-top" alt="...">
                  <div class="card-body">
                    <h5 class="card-title">New</h5>
                    <p class="card-text">500 Tk</p>
                    <a href="{{ route('product-details')}}" class="btn btn-primary">Add to Cart</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Shope Porducts End  -->

    <!-- pagination start  -->
    <div class="pagination text-center pb-4">
        <div class="container">
            <nav aria-label="Page navigation example">
                <ul class="pagination text-center">
                  <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                    </a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
                </ul>
              </nav>
        </div>
    </div>
    <!-- pagination end  -->

    <!-- Footer Area Start -->
</x-layouts>